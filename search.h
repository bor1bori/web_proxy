# include <limits.h> 
# include <string.h> 
# include <stdio.h> 
  
# define NO_OF_CHARS 256 
  
// A utility function to get maximum of two integers 
int max (int a, int b);
  
// The preprocessing function for Boyer Moore's 
// bad character heuristic 
void makeBadCharMat( const char *str, int size,  
                        int badchar[NO_OF_CHARS]) ;

/* A pattern searching function that uses Bad 
   Character Heuristic of Boyer Moore Algorithm */
char* boyerMooreSearchWithBadCharMat( char *txt,  const char *pat, int badchar[NO_OF_CHARS]);
/* Driver program to test above function */
