#include <stdio.h> // for perror
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <sys/types.h>
#include <netdb.h>
#include "search.h"
#include <thread>
#include <iostream>
#include <list>
#include <mutex>
#include <vector>

using namespace std;

int badchar_host[NO_OF_CHARS];

void usage() {
	printf("usage: web_proxy <tcp_port> [ssl_port] \nexample: web_proxy 8080 4433\n");
}

int relay(int srcfd, int dstfd) {
	const static int BUFSIZE = 2048;
	char buf[BUFSIZE];
	ssize_t received;
	printf("relay start from %d to %d\n", srcfd, dstfd);
	while (true) {
		received = recv(srcfd, buf, BUFSIZE - 1, 0);
		if (received == 0 || received == -1) {
			perror("recv failed");
			return -1;
		}
		buf[received] = '\0';
		printf("%s\n", buf);

		ssize_t sent = send(dstfd, buf, strlen(buf), 0);
		if (sent == 0) {
			perror("send failed");
			return -1;
		}
	}
}

int distributor(int clientfd) {
	const static int BUFSIZE = 1024;
	char buf[BUFSIZE];
	ssize_t received = recv(clientfd, buf, BUFSIZE - 1, 0);
	if (received == 0 || received == -1) {
		perror("recv failed");
		return -1;
	}
	buf[received] = '\0';
	char* hostname_ptr  = boyerMooreSearchWithBadCharMat((char *)buf, "\r\nHost: ", badchar_host) + 8;
	if (hostname_ptr == NULL) {
		return -1;
	}
	char *hostname_end = strstr(hostname_ptr, "\r\n");
	int hostname_length = (int)(hostname_end - hostname_ptr);
	char *hostname = (char*)malloc(hostname_length + 1);
	memset(hostname, 0, sizeof(hostname));
	memcpy(hostname, hostname_ptr, hostname_length);
	printf("%d Hostname: %s\n", clientfd, hostname);
	int status;
	int port;
	struct addrinfo hints;
	struct addrinfo *servinfo; // 결과를 저장할 변수

	memset(&hints, 0, sizeof(hints)); // hints 구조체의 모든 값을 0으로 초기화
	hints.ai_family = AF_INET; // IPv4 받겠다
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	status = getaddrinfo(hostname, "80", &hints, &servinfo);
	if (status != 0) {
		return -1;
	}

	int ip = 0;	
	for (int i = 0 ; i < 4 ; i++) {
		ip = ip << 8;
		ip += (int)servinfo->ai_addr->sa_data[i + 2];
	}
	ip += 0x01000000;
	int serverfd = socket(AF_INET, SOCK_STREAM, 0);
	if (serverfd == -1) {
		perror("socket failed");
		return -1;
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(80);
	addr.sin_addr.s_addr = htonl(ip);
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	int res = connect(serverfd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(struct sockaddr));
	if (res == -1) {
		perror("connect failed");
		return -1;
	}
	ssize_t sent = send(serverfd, buf, strlen(buf), 0);
		if (sent == 0) {
			perror("send failed");
			return -1;
		}
	thread t2(&relay, serverfd, clientfd);
	thread t1(&relay, clientfd, serverfd);
	t1.join();
	t2.join();
}

int main(int argc, char* argv[]) {
	list<thread*> threads;
	int tcp_port = 0;
	if (!(argc == 2 || argc == 3)) {
		usage();
		return -1;
	}
	tcp_port = atoi(argv[1]);
	if (tcp_port == 0) {
		usage();
		return -1;
	}

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket failed");
		return -1;
	}

	makeBadCharMat("\r\nHost: ", strlen("\r\nHost: "), badchar_host);

	int optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,  &optval , sizeof(int));

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(tcp_port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	int res = bind(sockfd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(struct sockaddr));
	if (res == -1) {
		perror("bind failed");
		return -1;
	}

	res = listen(sockfd, 2);
	if (res == -1) {
		perror("listen failed");
		return -1;
	}
	while (true) {
		struct sockaddr_in addr;
		socklen_t clientlen = sizeof(sockaddr);
		int client = accept(sockfd, reinterpret_cast<struct sockaddr*>(&addr), &clientlen);
		if (client < 0) {
			perror("ERROR on accept");
			break;
		}
		printf("connected\n");
		distributor(client);
	}

//	for (list<thread*>::iterator it = threads.begin() ; it != threads.end() ; it++) {
//		(*it)->join();
//	}

	close(sockfd);
}
