all: web_proxy

web_proxy: main.o search.o
	g++ -o web_proxy main.o search.o -lpthread
search.o:
	g++ -g -c -o search.o search.cpp
main.o:
	g++ -g -c -o main.o main.cpp
clean:
	rm web_proxy
	rm *.o
	